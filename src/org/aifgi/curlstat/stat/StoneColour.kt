package org.aifgi.curlstat.stat

/**
 * @author Alexey Ivanov
 */
enum class StoneColour {
    RED,
    YELLOW
}