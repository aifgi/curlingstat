package org.aifgi.curlstat.stat

/**
 * @author Alexey Ivanov
 */
data class Team(val name: String)

data class Game(val teamRed: Team, val teamYellow: Team, val hammerInTheFirst: StoneColour)

data class GameResult(val game: Game, val ends: List<End>)
