package org.aifgi.curlstat.stat

/**
 * @author Alexey Ivanov
 */
class GameResultBuilder(private val game: Game) {
    private var hammer = game.hammerInTheFirst
    private val ends = ArrayList<End>(11)

    fun addEnd(red: Int, yellow: Int): GameResultBuilder {
        if ((red != 0 && yellow != 0) || (red > 8) || (yellow > 8) || (red < 0) || (yellow < 0)) {
            throw IllegalStateException("Illegal score $red:$yellow")
        }
        val end = End(game, hammer, red, yellow)
        ends.add(end)

        if (red != 0) {
            hammer = StoneColour.YELLOW
        } else if (yellow != 0) {
            hammer = StoneColour.RED
        }
        return this
    }

    fun build() = GameResult(game, ends)
}
