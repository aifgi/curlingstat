package org.aifgi.curlstat.stat

/**
 * @author Alexey Ivanov
 */
class End(private val game: Game, private val hammer: StoneColour,
          private val resultRed: Int, private val resultYellow: Int) {
    fun isBlanked() = (resultYellow == 0) && (resultRed == 0)

    fun isStolen() = !isBlanked() && (if (hammer == StoneColour.YELLOW) resultRed != 0 else resultYellow != 0)

    fun isForced() = if (hammer == StoneColour.YELLOW) resultYellow == 1 else resultRed == 1

    fun isHammerUsed() = if (hammer == StoneColour.YELLOW) resultYellow >= 2 else resultRed >= 2

    fun doesTeamHaveHummer(team: Team) = if (hammer == StoneColour.YELLOW) team == game.teamYellow else team == game.teamRed

    fun getScore() = if (resultYellow != 0) resultYellow else resultRed

    override fun toString(): String {
        return "$resultRed:$resultYellow"
    }
}
