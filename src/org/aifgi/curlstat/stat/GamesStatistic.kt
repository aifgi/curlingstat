package org.aifgi.curlstat.stat

/**
 * @author Alexey Ivanov
 */
class GamesStatistic(val team: Team) {
    private var tookMultipleWithHammer: Int = 0
    private var force: Int = 0
    private var stolenFor: Int = 0
    private var stolenAgainst: Int = 0
    private var notBlankedEndsWithHammer: Int = 0
    private var notBlankedAndNotStolenEndsWithoutHammer: Int = 0
    private var endsWithoutHammer: Int = 0
    private var endsWithHammer: Int = 0

    private var pointsScoredFor: Int = 0
    private var pointsScoredAgainst: Int = 0
    private var endsScoredFor: Int = 0
    private var endsScoredAgainst: Int = 0

    fun hammerEfficiency() = tookMultipleWithHammer.toFloat() / notBlankedEndsWithHammer

    fun forceEfficiency() = force.toFloat() / notBlankedAndNotStolenEndsWithoutHammer

    fun stealEfficiency() = stolenFor.toFloat() / endsWithoutHammer

    fun stealDefence() = 1 - stolenAgainst.toFloat() / endsWithHammer

    fun pointForPerEnd() = pointsScoredFor.toFloat() / endsScoredFor

    fun pointAgianstPerEnd() = pointsScoredAgainst.toFloat() / endsScoredAgainst

    fun addToStat(gameResult: GameResult) {
        for (end in gameResult.ends) {
            val hasHammer = end.doesTeamHaveHummer(team)
            val blanked = end.isBlanked()
            val hammerUsed = end.isHammerUsed()
            val forced = end.isForced()
            val stolen = end.isStolen()
            val score = end.getScore()
            if (hasHammer) {
                ++endsWithHammer
                if (!blanked) {
                    ++notBlankedEndsWithHammer
                }
                if (!stolen) {
                    ++stolenAgainst
                }
                if (hammerUsed) {
                    ++tookMultipleWithHammer
                }
            } else {
                ++endsWithoutHammer
                if (!blanked and !stolen) {
                    ++notBlankedAndNotStolenEndsWithoutHammer
                }
                if (stolen) {
                    ++stolenFor
                }
                if (forced) {
                    ++force
                }
            }
            if (!blanked) {
                if (hasHammer == stolen) {
                    ++endsScoredAgainst
                    pointsScoredAgainst += score
                } else {
                    ++endsScoredFor
                    pointsScoredFor += score
                }
            }
        }
    }

    override fun toString(): String {
        return "GameStatistic(HE: ${hammerEfficiency()}, SD: ${stealDefence()}, FE: ${forceEfficiency()}, SE: ${stealEfficiency()}, PF/E: ${pointForPerEnd()}, PA/E: ${pointAgianstPerEnd()})"
    }


}