package org.aifgi.curlstat.parser

import org.aifgi.curlstat.stat.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.io.InputStream

/**
 * @author Alexey Ivanov
 */
operator fun Element.get(tagId: String): Elements = this.getElementsByTag(tagId)

class CurlingRuParser {
    fun parse(inputStream: InputStream): Map<Team, List<GameResult>> {
        val document = Jsoup.parse(inputStream, "UTF-8", "curling.ru")
        val resultTables = document.select("div.bl-rezult-container-content table.table-fix") ?: throw IllegalStateException("No tables with results")

        val results = HashMap<Team, MutableList<GameResult>>()
        resultTables.forEach { table ->
            val gameResult = parseResultTable(table)
            val game = gameResult.game

            results.computeIfAbsent(game.teamYellow) {
                ArrayList()
            }
                .add(gameResult)

            results.computeIfAbsent(game.teamRed) {
                ArrayList()
            }
                .add(gameResult)
        }
        return results
    }

    private fun parseResultTable(table: Element): GameResult {
        val tableBodies = table["tbody"]
        if (tableBodies.size != 1) {
            throw IllegalStateException("There is no tbody")
        }

        val trs = tableBodies.first()["tr"]
        if (trs.size != 3) {
            throw IllegalStateException("Incorrect number of rows in table")
        }

        val (firstColumn, lastColumn) = checkColumns(trs[0]["td"])

        val firstTeamCells = trs[1]["td"]
        val secondTeamCells = trs[2]["td"]
        if (firstTeamCells.size != secondTeamCells.size) {
            throw IllegalStateException("Incorrect number of tds")
        }

        val game = extractGame(firstTeamCells, secondTeamCells)

        val gameResultBuilder = GameResultBuilder(game)
        for (i in firstColumn..lastColumn) {
            val firstTeamScoreText = firstTeamCells[i].text()
            val secondTeamScoreText = secondTeamCells[i].text()
            if (firstTeamScoreText == "X" && firstTeamScoreText == secondTeamScoreText) {
                break
            }
            if (firstTeamScoreText.isBlank() && secondTeamScoreText.isBlank()) {
                continue
            }

            gameResultBuilder.addEnd(Integer.parseInt(firstTeamScoreText), Integer.parseInt(secondTeamScoreText))
        }

        return gameResultBuilder.build()
    }

    private fun checkColumns(columns: Elements): Pair<Int, Int> {
        var firstColumn: Int = -1
        for (i in 2..(columns.size - 1)) {
            if (columns[i].text() == "1") {
                firstColumn = i
                break
            }
        }
        var lastColumn: Int = -1
        for (i in firstColumn..(columns.size - 1)) {
            if (columns[i].text() == "Итого") {
                lastColumn = i - 1
                break
            }
        }
        if (firstColumn < 0 || lastColumn < 0 || firstColumn >= lastColumn) {
            throw IllegalStateException("Incorrect columns")
        }
        return Pair(firstColumn, lastColumn)
    }

    private fun extractGame(firstTeamCells: Elements, secondTeamCells: Elements): Game {
        val firstTeamName = getTeamName(firstTeamCells)
        val secondTeamName = getTeamName(secondTeamCells)

        val firstHammer = firstTeamCells[1].text()
        val secondHammer = secondTeamCells[1].text()

        val hammer = when {
            firstHammer == "*" -> StoneColour.RED
            secondHammer == "*" -> StoneColour.YELLOW
            else -> throw IllegalStateException("Incorrect number teams with hammer")
        }

        return Game(Team(firstTeamName), Team(secondTeamName), hammer)
    }

    private fun getTeamName(firstTeamCells: Elements): String {
        val text = firstTeamCells[0].text()
        return text.replace(Regex("\\s?+[\\-–]\\s?+"), "-").trim()
    }
}