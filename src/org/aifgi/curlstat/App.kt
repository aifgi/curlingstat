package org.aifgi.curlstat

import org.aifgi.curlstat.parser.CurlingRuParser
import org.aifgi.curlstat.stat.GamesStatistic
import java.net.URL

/**
 * @author Alexey Ivanov
 */
fun main(args: Array<String>) {
    if (args.isEmpty()) {
        throw IllegalStateException("First argument should be URL")
    }
    val url = URL(args[0])
    val parser = CurlingRuParser()
    val results = url.openStream().use {
        parser.parse(it)
    }
    println("Team,Hammer Efficiency,Steal Defence Efficiency,Force Efficiency,Steal Efficiency,PF/E,PA/E")
    for ((team, gameResults) in results) {
        val statistic = GamesStatistic(team)
        for (gameResult in gameResults) {
            statistic.addToStat(gameResult)
        }
        println("${team.name},${statistic.hammerEfficiency()},${statistic.stealDefence()},${statistic.forceEfficiency()}," +
                "${statistic.stealEfficiency()},${statistic.pointForPerEnd()},${statistic.pointAgianstPerEnd()}")
    }
}